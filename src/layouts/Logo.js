import { ReactComponent as LogoDark } from "../assets/images/logos/monsterlogo.svg";
import biologo from "../assets/images/logos/biogazlogo.png";
import { Link } from "react-router-dom";

const Logo = () => {
  return (
    <Link to="/">
      {/* <LogoDark /> */}
      <img src={biologo} width="70%" />
    </Link>
  );
};

export default Logo;
