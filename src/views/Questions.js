export const questions = [
  {
    section: 1,
    items: [
      {
        id: "selectPaille",
        label: "Paille d'Avoine",
        type: "select",
        value: 'Paille d"Avoine',
        options: ["Fumier de bovins pailleux", "Substrat"],
      },
      {
        id: "textPaille",
        label: "Paille d'Avoine",
        type: "number",
        value: 'Paille d"Avoine',
      },
      {
        id: "selectAlgues",
        label: "Algues",
        type: "select",
        value: "Algues",
        options: ["Op1", "Op2"],
      },
      {
        id: "textAlgues",
        label: "Algues",
        type: "number",
        value: "Algues",
      },
      {
        id: "selectFoin",
        label: "Foin",
        type: "select",
        value: "Foin",
        options: ["Op1", "Op2"],
      },
      {
        id: "textFoin",
        label: "Foin",
        type: "number",
        value: "Foin",
      },
    ],
  },
  {
    section: 2,
    items: [
      {
        id: "PailleResult",
        label: "PailleResult",
        type: "PailleResult",
        value: "PailleResult",
      },
      {
        id: "AlguesResult",
        label: "AlguesResult",
        type: "AlguesResult",
        value: "AlguesResult",
      },
      {
        id: "FoinResult",
        label: "FoinResult",
        type: "FoinResult",
        value: "FoinResult",
      },
      {
        id: "TotalOne",
        label: "TotalOne",
        type: "TotalOne",
        value: "TotalOne",
      },
      {
        id: "TotalTwo",
        label: "TotalTwo",
        type: "TotalTwo",
        value: "TotalTwo",
      },
      {
        id: "TotalThree",
        label: "TotalThree",
        type: "TotalThree",
        value: "TotalThree",
      },
    ],
  },
  {
    section: 3,
    items: [
      {
        id: "temps",
        label: "Temps de fonctionnement du moteur",
        type: "temps",
        value: "Heures/an",
      },
      {
        id: "regime",
        label: "Regime de Temperature",
        type: "regime",
        value: "°C",
      },
      {
        id: "autonomie",
        label: "Autonomie",
        type: "autonomie",
        value: "Heure",
      },
    ],
  },
  {
    section: 4,
    items: [
      {
        id: "name",
        label: "name",
        type: "name",
        value: "name",
      },
      {
        id: "email",
        label: "email",
        type: "email",
        value: "email",
      },
      {
        id: "password",
        label: "password",
        type: "password",
        value: "password",
      },
      {
        id: "ConfirmPassword",
        label: "ConfirmPassword",
        type: "password",
        value: "ConfirmPassword",
      },
    ],
  },
];
