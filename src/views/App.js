import logo from "./logo.svg";
import "./App.css";
import { MultiStepProgressBar } from "./components/MultiStepProgressBar";
import { Container, Row, Col, Card, Button } from "react-bootstrap";
import { useState, useEffect } from "react";
import { MultiStepForm } from "./components/MultiStepForm";
import { questions } from "./Questions";
import email from "../assets/images/logos/Email.png";
import axios from "axios";
import jwt_decode from "jwt-decode";
import { useNavigate } from "react-router-dom";

function App() {
  const [index, setIndex] = useState(1);
  const [submitted, setSubmitted] = useState(false);
  const totalPagesCount = questions?.length || 0;
  // numbered by pages. for exampe { 1: [{"key" : "value"}], 2:["key": "value"], 3: []}
  const [pagesAnswers, setPagesAnswers] = useState({});
  const navigate = useNavigate();
  //CHECK LOGGED IN START
  const [name, setName] = useState("");
  const [token, setToken] = useState("");
  const [expire, setExpire] = useState("");
  const [users, setUsers] = useState([]);

  useEffect(() => {
    refreshToken();
    getUsers();
  }, []);

  const refreshToken = async () => {
    try {
      const response = await axios.post("http://localhost:5000/login", {
        email: localStorage.getItem("loggedEmail"),
        password: localStorage.getItem("loggedPassword"),
      });
      setToken(response.data.accessToken);
      const decoded = jwt_decode(response.data.accessToken);
      setName(decoded.name);
      setExpire(decoded.exp);
      console.log("MY TOKEN", response.data.accessToken);
    } catch (error) {
      if (error.response) {
        navigate("/login");
      }
    }
  };

  const axiosJWT = axios.create();

  axiosJWT.interceptors.request.use(
    async (config) => {
      const currentDate = new Date();
      if (expire * 1000 < currentDate.getTime()) {
        const response = await axios.post("http://localhost:5000/login", {
          email: localStorage.getItem("loggedEmail"),
          password: localStorage.getItem("loggedPassword"),
        });
        config.headers.Authorization = `Bearer ${response.data.accessToken}`;
        setToken(response.data.accessToken);
        const decoded = jwt_decode(response.data.accessToken);
        setName(decoded.name);
        setExpire(decoded.exp);
      }
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );

  const getUsers = async () => {
    const response = await axiosJWT.get("http://localhost:5000/users", {
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
    setUsers(response.data);
  };
  //CHECK LOGGED IN END

  const checkErrors = () => {
    // check errors in first page
    if (index === 1) {
      if (
        localStorage.getItem("temporaryPaille").length != 0 &&
        localStorage.getItem("temporaryAlgues").length != 0 &&
        localStorage.getItem("temporaryFoin").length != 0 &&
        localStorage.getItem("SelectedPailleValue") != "0" &&
        localStorage.getItem("SelectedAlguesValue") != "0" &&
        localStorage.getItem("SelectedFoinValue") != "0"
      ) {
        nextButton();
        localStorage.setItem("temporaryPaille", "");
        localStorage.setItem("temporaryAlgues", "");
        localStorage.setItem("temporaryFoin", "");
        localStorage.setItem("SelectedPailleValue", "0");
        localStorage.setItem("SelectedAlguesValue", "0");
        localStorage.setItem("SelectedFoinValue", "0");
      }
    } else if (index === 3) {
      if (
        localStorage.getItem("temporaryTemps").length != 0 &&
        localStorage.getItem("temporaryRegime").length != 0 &&
        localStorage.getItem("temporaryAutonomie").length != 0
      ) {
        nextButton();
        localStorage.setItem("temporaryTemps", "");
        localStorage.setItem("temporaryRegime", "");
        localStorage.setItem("temporaryAutonomie", "");
      }
    } else if (index === 4) {
      if (
        localStorage.getItem("userName").length != 0 &&
        localStorage.getItem("userEmail").length != 0 &&
        localStorage.getItem("userPassword").length != 0 &&
        localStorage.getItem("userPasswordConfirm").length != 0 &&
        localStorage.getItem("userPassword") ===
          localStorage.getItem("userPasswordConfirm")
      ) {
        nextButton();
        localStorage.setItem("temporaryUserName", "");
        localStorage.setItem("temporaryUserEmail", "");
        localStorage.setItem("temporaryUserPassword", "");
        localStorage.setItem("temporaryUserPasswordConfirm", "");
      }
    } else {
      nextButton();
    }
  };

  const prevButton = () => {
    if (index > 1) {
      setIndex((prevIndex) => prevIndex - 1);
    }
  };

  const nextButton = () => {
    if (index - 4) {
      setIndex((prevIndex) => prevIndex + 1);
    } else {
      // clear the form on submit
      setPagesAnswers({});
      setSubmitted(true);
    }
  };

  const onPageAnswerUpdate = (step, answersObj) => {
    setPagesAnswers({ ...pagesAnswers, [step]: answersObj });
  };

  const handleStart = () => {
    setIndex(1);
    setSubmitted(false);
  };

  // useEffect(() => {
  //   getBook();
  // }, []);

  // const [book, setBook] = useState([]);

  // const getBook = async (dispatch) => {
  //   try {
  //     const response = await fetch(`http://localhost:3001/home`, {
  //       method: "GET",

  //       headers: {
  //         "Content-Type": "application/json",
  //         Authorization: "Bearer " + localStorage.getItem("access_token"),
  //       },
  //     })
  //       .then((response) => response.json())
  //       .then((data) => setBook(data));
  //   } catch (error) {
  //     console.log(error);
  //   }
  // };

  // const SendDataToAPI = async (dispatch) => {
  //   try {
  //     const response = await fetch("http://localhost:3001/create", {
  //       method: "POST",

  //       headers: {
  //         "Content-Type": "application/json",
  //         Authorization: "Bearer " + localStorage.getItem("access_token"),
  //       },
  //       body: JSON.stringify({
  //         userName: localStorage.getItem("userName"),
  //         userEmail: localStorage.getItem("userEmail"),
  //         userPassword: localStorage.getItem("userPassword"),
  //       }),
  //     })
  //       .then((response) => response.json())
  //       .then((data) => setBook(data));
  //   } catch (error) {
  //     console.log(error);
  //   }
  //   getBook();
  // };
  // if (submitted) {
  //   SendDataToAPI();
  //   console.log("SUBMITTED");
  // }
  return (
    <div className="App">
      <Container className="h-100">
        <Row className="m-5">
          <Col className="align-self-center">
            <MultiStepProgressBar step={index} />
          </Col>
        </Row>
        <Row>
          {submitted ? (
            <Card>
              <Card.Body>
                <img src={email} width="20%"></img>
                <h4>
                  Un email a été envoyé à {localStorage.getItem("userEmail")}
                </h4>{" "}
                <h4>Veuillez vérifier votre boîte de réception.</h4>
              </Card.Body>
              <Card.Footer>
                <Button onClick={handleStart}>OK</Button>
              </Card.Footer>
            </Card>
          ) : (
            <Card>
              <Card.Body>
                <MultiStepForm
                  list={questions}
                  step={index}
                  onPageUpdate={onPageAnswerUpdate}
                  pagesAnswers={pagesAnswers}
                />
              </Card.Body>
              <Card.Footer className="d-flex justify-content-between">
                <Button onClick={prevButton} disabled={index == 1}>
                  Retour
                </Button>
                <Button onClick={checkErrors} type="submit" id="nextSubmitBtn">
                  {index == totalPagesCount ? "Submit" : "Suivant"}
                </Button>
              </Card.Footer>
            </Card>
          )}
        </Row>
      </Container>
    </div>
  );
}

export default App;
