import { Form } from "react-bootstrap";
import { useState } from "react";

import cereale from "./imgs/cereale.png";
import algue from "./imgs/algue.png";
import foin from "./imgs/foin.png";

export const FormItem = ({ item, onChange, answer }) => {
  const [currentValue, setCurrentValue] = useState(answer || null);

  //INITIALIZING VALUES START
  //localStorage.setItem("SelectedPailleValue", "0");
  //localStorage.setItem("paille", "");
  localStorage.setItem("temporaryPaille", "");

  //localStorage.setItem("SelectedAlguesValue", "0");
  //localStorage.setItem("algues", "");
  localStorage.setItem("temporaryAlgues", "");

  //localStorage.setItem("SelectedFoinValue", "0");
  //localStorage.setItem("foin", "");
  localStorage.setItem("temporaryFoin", "");

  //localStorage.setItem("temps", "");
  localStorage.setItem("temporaryTemps", "");

  //localStorage.setItem("regime", "");
  localStorage.setItem("temporaryRegime", "");

  //localStorage.setItem("autonomie", "");
  localStorage.setItem("temporaryAutonomie", "");
  //INITIALIZING VALUE END

  const handleChange = (value) => {
    setCurrentValue(value);
    onChange(value, item.value);
    localStorage.setItem("EXX", value);
  };
  const handleSelectPailleChange = (value) => {
    localStorage.setItem("SelectedPailleValue", value);
    document
      .getElementById("errorTextSelectPaille")
      .classList.add("hideErrorMsg");
    if (value === "0") {
      document.getElementById("selectPaille").classList.add("errorBorder");
      document
        .getElementById("errorTextSelectPaille")
        .classList.add("showErrorMsg");
      document
        .getElementById("errorTextSelectPaille")
        .classList.remove("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.add("disabledBtn");
    } else {
      document.getElementById("selectPaille").classList.remove("errorBorder");
      document
        .getElementById("errorTextSelectPaille")
        .classList.remove("showErrorMsg");
      document
        .getElementById("errorTextSelectPaille")
        .classList.add("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.remove("disabledBtn");
    }
  };
  const handlePailleChange = (value) => {
    localStorage.setItem("paille", value);
    localStorage.setItem("temporaryPaille", localStorage.getItem("paille"));
    document.getElementById("errorTextPaille").classList.add("hideErrorMsg");

    if (localStorage.getItem("paille").length === 0) {
      document.getElementById("textPaille").classList.add("errorBorder");
      document.getElementById("errorTextPaille").classList.add("showErrorMsg");
      document
        .getElementById("errorTextPaille")
        .classList.remove("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.add("disabledBtn");
    } else {
      document.getElementById("textPaille").classList.remove("errorBorder");
      document
        .getElementById("errorTextPaille")
        .classList.remove("showErrorMsg");
      document.getElementById("errorTextPaille").classList.add("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.remove("disabledBtn");
    }
  };
  const handleSelectAlguesChange = (value) => {
    localStorage.setItem("SelectedAlguesValue", value);
    document
      .getElementById("errorTextSelectAlgues")
      .classList.add("hideErrorMsg");
    if (value === "0") {
      document.getElementById("selectAlgues").classList.add("errorBorder");
      document
        .getElementById("errorTextSelectAlgues")
        .classList.add("showErrorMsg");
      document
        .getElementById("errorTextSelectAlgues")
        .classList.remove("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.add("disabledBtn");
    } else {
      document.getElementById("selectAlgues").classList.remove("errorBorder");
      document
        .getElementById("errorTextSelectAlgues")
        .classList.remove("showErrorMsg");
      document
        .getElementById("errorTextSelectAlgues")
        .classList.add("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.remove("disabledBtn");
    }
  };
  const handleAlguesChange = (value) => {
    localStorage.setItem("algues", value);
    localStorage.setItem("temporaryAlgues", localStorage.getItem("algues"));
    document.getElementById("errorTextAlgues").classList.add("hideErrorMsg");
    if (localStorage.getItem("algues").length === 0) {
      document.getElementById("textAlgues").classList.add("errorBorder");
      document.getElementById("errorTextAlgues").classList.add("showErrorMsg");
      document
        .getElementById("errorTextAlgues")
        .classList.remove("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.add("disabledBtn");
    } else {
      document.getElementById("textAlgues").classList.remove("errorBorder");
      document
        .getElementById("errorTextAlgues")
        .classList.remove("showErrorMsg");
      document.getElementById("errorTextAlgues").classList.add("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.remove("disabledBtn");
    }
  };
  const handleSelectFoinChange = (value) => {
    localStorage.setItem("SelectedFoinValue", value);
    document
      .getElementById("errorTextSelectFoin")
      .classList.add("hideErrorMsg");
    if (value === "0") {
      document.getElementById("selectFoin").classList.add("errorBorder");
      document
        .getElementById("errorTextSelectFoin")
        .classList.add("showErrorMsg");
      document
        .getElementById("errorTextSelectFoin")
        .classList.remove("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.add("disabledBtn");
    } else {
      document.getElementById("selectFoin").classList.remove("errorBorder");
      document
        .getElementById("errorTextSelectFoin")
        .classList.remove("showErrorMsg");
      document
        .getElementById("errorTextSelectFoin")
        .classList.add("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.remove("disabledBtn");
    }
  };
  const handleFoinChange = (value) => {
    localStorage.setItem("foin", value);
    localStorage.setItem("temporaryFoin", localStorage.getItem("foin"));
    document.getElementById("errorTextFoin").classList.add("hideErrorMsg");
    if (localStorage.getItem("foin").length === 0) {
      document.getElementById("textFoin").classList.add("errorBorder");
      document.getElementById("errorTextFoin").classList.add("showErrorMsg");
      document.getElementById("errorTextFoin").classList.remove("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.add("disabledBtn");
    } else {
      document.getElementById("textFoin").classList.remove("errorBorder");
      document.getElementById("errorTextFoin").classList.remove("showErrorMsg");
      document.getElementById("errorTextFoin").classList.add("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.remove("disabledBtn");
    }
  };
  const handleTempsChange = (value) => {
    localStorage.setItem("temps", value);
    localStorage.setItem("temporaryTemps", localStorage.getItem("temps"));
    document.getElementById("errorTextTemps").classList.add("hideErrorMsg");
    if (localStorage.getItem("temps").length === 0) {
      document.getElementById("temps").classList.add("errorBorder");
      document.getElementById("errorTextTemps").classList.add("showErrorMsg");
      document
        .getElementById("errorTextTemps")
        .classList.remove("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.add("disabledBtn");
    } else {
      document.getElementById("temps").classList.remove("errorBorder");
      document
        .getElementById("errorTextTemps")
        .classList.remove("showErrorMsg");
      document.getElementById("errorTextTemps").classList.add("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.remove("disabledBtn");
    }
  };
  const handleRegimeChange = (value) => {
    localStorage.setItem("regime", value);
    localStorage.setItem("temporaryRegime", localStorage.getItem("regime"));
    document.getElementById("errorTextRegime").classList.add("hideErrorMsg");
    if (localStorage.getItem("regime").length === 0) {
      document.getElementById("regime").classList.add("errorBorder");
      document.getElementById("errorTextRegime").classList.add("showErrorMsg");
      document
        .getElementById("errorTextRegime")
        .classList.remove("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.add("disabledBtn");
    } else {
      document.getElementById("regime").classList.remove("errorBorder");
      document
        .getElementById("errorTextRegime")
        .classList.remove("showErrorMsg");
      document.getElementById("errorTextRegime").classList.add("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.remove("disabledBtn");
    }
  };
  const handleAutonomieChange = (value) => {
    localStorage.setItem("autonomie", value);
    localStorage.setItem(
      "temporaryAutonomie",
      localStorage.getItem("autonomie")
    );
    document.getElementById("errorTextAutonomie").classList.add("hideErrorMsg");
    if (localStorage.getItem("autonomie").length === 0) {
      document.getElementById("autonomie").classList.add("errorBorder");
      document
        .getElementById("errorTextAutonomie")
        .classList.add("showErrorMsg");
      document
        .getElementById("errorTextAutonomie")
        .classList.remove("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.add("disabledBtn");
    } else {
      document.getElementById("autonomie").classList.remove("errorBorder");
      document
        .getElementById("errorTextAutonomie")
        .classList.remove("showErrorMsg");
      document
        .getElementById("errorTextAutonomie")
        .classList.add("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.remove("disabledBtn");
    }
  };
  const handleNameChange = (value) => {
    localStorage.setItem("userName", value);
    localStorage.setItem("temporaryUserName", localStorage.getItem("userName"));
    document.getElementById("errorTextUserName").classList.add("hideErrorMsg");
    if (localStorage.getItem("userName").length === 0) {
      document.getElementById("name").classList.add("errorBorder");
      document
        .getElementById("errorTextUserName")
        .classList.add("showErrorMsg");
      document
        .getElementById("errorTextUserName")
        .classList.remove("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.add("disabledBtn");
    } else {
      document.getElementById("name").classList.remove("errorBorder");
      document
        .getElementById("errorTextUserName")
        .classList.remove("showErrorMsg");
      document
        .getElementById("errorTextUserName")
        .classList.add("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.remove("disabledBtn");
    }
  };
  const handleEmailChange = (value) => {
    localStorage.setItem("userEmail", value);
    localStorage.setItem(
      "temporaryUserEmail",
      localStorage.getItem("userEmail")
    );
    document.getElementById("errorTextUserEmail").classList.add("hideErrorMsg");
    if (localStorage.getItem("userEmail").length === 0) {
      document.getElementById("email").classList.add("errorBorder");
      document
        .getElementById("errorTextUserEmail")
        .classList.add("showErrorMsg");
      document
        .getElementById("errorTextUserEmail")
        .classList.remove("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.add("disabledBtn");
    } else {
      document.getElementById("email").classList.remove("errorBorder");
      document
        .getElementById("errorTextUserEmail")
        .classList.remove("showErrorMsg");
      document
        .getElementById("errorTextUserEmail")
        .classList.add("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.remove("disabledBtn");
    }
  };
  const handlePasswordChange = (value) => {
    localStorage.setItem("userPassword", value);
    localStorage.setItem(
      "temporaryUserPassword",
      localStorage.getItem("userPassword")
    );
    document
      .getElementById("errorTextUserPassword")
      .classList.add("hideErrorMsg");
    if (localStorage.getItem("userPassword").length === 0) {
      document.getElementById("password").classList.add("errorBorder");
      document
        .getElementById("errorTextUserPassword")
        .classList.add("showErrorMsg");
      document
        .getElementById("errorTextUserPassword")
        .classList.remove("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.add("disabledBtn");
    } else {
      document.getElementById("password").classList.remove("errorBorder");
      document
        .getElementById("errorTextUserPassword")
        .classList.remove("showErrorMsg");
      document
        .getElementById("errorTextUserPassword")
        .classList.add("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.remove("disabledBtn");
    }
  };
  const handleConfirmPasswordChange = (value) => {
    localStorage.setItem("userPasswordConfirm", value);
    localStorage.setItem(
      "temporaryUserPasswordConfirm",
      localStorage.getItem("userPasswordConfirm")
    );
    document
      .getElementById("errorTextUserPasswordConfirm")
      .classList.add("hideErrorMsg");
    if (localStorage.getItem("userPasswordConfirm").length === 0) {
      document.getElementById("ConfirmPassword").classList.add("errorBorder");
      document
        .getElementById("errorTextUserPasswordConfirm")
        .classList.add("showErrorMsg");
      document
        .getElementById("errorTextUserPasswordConfirm")
        .classList.remove("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.add("disabledBtn");
    } else {
      document
        .getElementById("ConfirmPassword")
        .classList.remove("errorBorder");
      document
        .getElementById("errorTextUserPasswordConfirm")
        .classList.remove("showErrorMsg");
      document
        .getElementById("errorTextUserPasswordConfirm")
        .classList.add("hideErrorMsg");
      document.getElementById("nextSubmitBtn").classList.remove("disabledBtn");
    }
  };

  // if (localStorage.getItem("errorPaille").toString() === "true") {
  //   var error = true;
  // } else {
  //   var error = false;
  // }

  switch (item.id) {
    case "selectPaille":
      return (
        <>
          <div id="FirstResultsTextUp">
            <h3>Substrats annuel</h3>
            <h3>à traiter</h3>
            <h5>Tonnes/an</h5>
          </div>
          <Form.Label>{item.label}</Form.Label>

          <select
            class="form-select"
            aria-label="Default select example"
            id="selectPaille"
            required
            onChange={(e) =>
              handleSelectPailleChange(e.target.value, item.value)
            }
          >
            <option selected disabled value="0">
              Choisir...
            </option>
            <option value="1">Fumier de bovins pailleux</option>
            <option value="2">Fumier de porcs</option>
            <option value="3">Fiente de volaillles pailleux</option>
            <option value="4">Fumier de mouton</option>
            <option value="5">Exctrèments de chevre</option>
            <option value="6">Fumier de cheval</option>
            <option value="7">Ensilage de Mais</option>
            <option value="8">Paille de Mais</option>
            <option value="9">Ensilage de Céréales</option>
            <option value="10">Grains de Céréales</option>
            <option value="11">Pailles de Céréales</option>
            <option value="12">Résidus de sèchage de céréales</option>
            <option value="13">Grain de blé</option>
            <option value="14">Paille de blé</option>
            <option value="15">Blé plante entière</option>
            <option value="16">Ensilage de triticale</option>
            <option value="17">Grains de triticale</option>
            <option value="18">Ensilage de seigle (WPS)</option>
            <option value="19">Paille de Seigle</option>
            <option value="20">Paille de Seigle (3 cm)</option>
            <option value="21">Paille de Seigle (0,1 - 2 cm)</option>
            <option value="22">Ensilage d'Avoine</option>
            <option value="23">Paille d'Avoine</option>
            <option value="24">Ensilage d'orge</option>
            <option value="25">Paille d'orge</option>
            <option value="26">Paille de Riz (3 cm)</option>
            <option value="27">Paille de Riz (0-0,2 cm)</option>
            <option value="28">Son de Riz</option>
            <option value="29">Bettrave Sucrière (23%MS)</option>
            <option value="30">
              Fannes de Betteraves fraiches ou ensilées
            </option>
            <option value="31">Ensilage de Bettraves Fourragères</option>
            <option value="32">Ensilage d'Herbe</option>
            <option value="33">Ensilage d'Herbe Grasse</option>
            <option value="34">Foin</option>
            <option value="35">Trefle</option>
            <option value="36">Ensilage de Sorgho sucrier</option>
            <option value="37">Ensilage de Sorgho lux Cycle</option>
            <option value="38">Paille de colza</option>
            <option value="39">Ensilage de colza (Riches en tiges)</option>
            <option value="40">Ensilage de Tournesol</option>
            <option value="41">Algues</option>
            <option value="42">Fannes de pommes de terre</option>
            <option value="43">Fumier</option>
            <option value="44">Fumier</option>
            <option value="45">Fumier</option>
            <option value="46">Fumier</option>
            <option value="47">Fumier</option>
            <option value="48">Fumier</option>
            <option value="49">Fumier</option>
            <option value="50">Fumier</option>
            <option value="51">Fumier</option>
            <option value="52">Fumier</option>
            <option value="53">Fumier</option>
            <option value="54">Fumier</option>
            <option value="55">Fumier</option>
            <option value="56">Fumier</option>
            <option value="57">Fumier</option>
            <option value="58">Fumier</option>
            <option value="59">Fumier</option>
            <option value="60">Fumier</option>
            <option value="61">Fumier</option>
            <option value="62">Fumier</option>
            <option value="63">Fumier</option>
            <option value="64">Fumier</option>
            <option value="65">Fumier</option>
            <option value="66">Fumier</option>
            <option value="67">Fumier</option>
            <option value="68">Fumier</option>
            <option value="69">Fumier</option>
            <option value="70">Fumier</option>
            <option value="71">Fumier</option>
            <option value="72">Fumier</option>
            <option value="73">Fumier</option>
            <option value="74">Fumier</option>
            <option value="75">Fumier</option>
            <option value="76">Fumier</option>
            <option value="77">Fumier</option>
            <option value="78">Fumier</option>
            <option value="79">Fumier</option>
            <option value="80">Fumier</option>
            <option value="81">Fumier</option>
            <option value="82">Fumier</option>
            <option value="83">Fumier</option>
            <option value="84">Fumier</option>
            <option value="85">Fumier</option>
            <option value="86">Fumier</option>
            <option value="87">Fumier</option>
            <option value="88">Fumier</option>
            <option value="89">Fumier</option>
            <option value="90">Fumier</option>
          </select>
          <div id="errorTextSelectPaille">
            <Form.Text style={{ color: "red" }}>Champs obligatoire</Form.Text>
          </div>
        </>
      );
      break;
    case "textPaille":
      return (
        <>
          <Form.Label>{item.label}</Form.Label>
          <div style={{ display: "flex" }}>
            <Form.Control
              // style={{ border: error ? "2px solid red" : "" }}
              type="number"
              id={item.id}
              onChange={(e) => handlePailleChange(e.target.value, item.value)}
              value={currentValue}
              required
            />
            <img src={cereale} />
          </div>
          <div id="errorTextPaille">
            <Form.Text style={{ color: "red" }}>Champs obligatoire</Form.Text>
          </div>

          {/* {(() => {
            if (localStorage.getItem("errorPaille").toString() === "true") {
              return (
                <Form.Text style={{ color: "red" }}>
                  This is a required field
                </Form.Text>
              );
            }
          })()} */}
        </>
      );
      break;
    case "selectAlgues":
      return (
        <>
          <Form.Label>{item.label}</Form.Label>

          <select
            class="form-select"
            aria-label="Default select example"
            id="selectAlgues"
            required
            onChange={(e) =>
              handleSelectAlguesChange(e.target.value, item.value)
            }
          >
            <option selected disabled value="0">
              Choisir...
            </option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>
          <div id="errorTextSelectAlgues">
            <Form.Text style={{ color: "red" }}>Champs obligatoire</Form.Text>
          </div>
        </>
      );
      break;
    case "textAlgues":
      return (
        <>
          <Form.Label>{item.label}</Form.Label>
          <div style={{ display: "flex" }}>
            <Form.Control
              type="number"
              id={item.id}
              onChange={(e) => handleAlguesChange(e.target.value, item.value)}
              value={currentValue}
              required
            />
            <img src={algue} />
          </div>
          <div id="errorTextAlgues">
            <Form.Text style={{ color: "red" }}>Champs obligatoire</Form.Text>
          </div>
        </>
      );
      break;
    case "selectFoin":
      return (
        <>
          <Form.Label>{item.label}</Form.Label>

          <select
            class="form-select"
            aria-label="Default select example"
            id="selectFoin"
            required
            onChange={(e) => handleSelectFoinChange(e.target.value, item.value)}
          >
            <option selected disabled value="0">
              Choisir...
            </option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>
          <div id="errorTextSelectFoin">
            <Form.Text style={{ color: "red" }}>Champs obligatoire</Form.Text>
          </div>
        </>
      );
      break;
    case "textFoin":
      return (
        <>
          <Form.Label>{item.label}</Form.Label>
          <div style={{ display: "flex" }}>
            <Form.Control
              type="number"
              id={item.id}
              onChange={(e) => handleFoinChange(e.target.value, item.value)}
              value={currentValue}
              required
            />
            <img src={foin} />
          </div>
          <div id="errorTextFoin">
            <Form.Text style={{ color: "red" }}>Champs obligatoire</Form.Text>
          </div>
        </>
      );
      break;
    case "selectFoin":
      return (
        <>
          <Form.Label>{item.label}</Form.Label>

          <select
            class="form-select"
            aria-label="Default select example"
            required
          >
            <option selected disabled value="">
              Choisir...
            </option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
          </select>
        </>
      );
      break;
    case "PailleResult":
      return (
        <>
          <div id="FirstResultsTextUp">
            <h3>Production annuelle </h3>
            <h3>en Biogaz</h3>
            <h5>Nm3 Biogaz/an</h5>
          </div>

          <table id="FirstResults">
            <tr
              style={{
                backgroundColor: "#B0C220",
                color: "white",
                borderRadius: 20,
              }}
            >
              <td>Paille d'avoine</td>
              <td>{localStorage.getItem("paille")}</td>
            </tr>
          </table>
          {/* <Form.Control
            type="PailleResult"
            id={item.label}
            onChange={(e) => handleChange(e.target.value, item.value)}
            value={localStorage.getItem("paille")}
            style={{ backgroundColor: "#B0C220" }}
            disabled readonly
          /> */}
        </>
      );
      break;
    case "AlguesResult":
      return (
        <>
          <table id="FirstResults">
            <tr
              style={{
                backgroundColor: "#B0C220",
                color: "white",
                borderRadius: 20,
              }}
            >
              <td>Paille d'avoine</td>
              <td>{localStorage.getItem("algues")}</td>
            </tr>
          </table>
          {/* <Form.Control
            type="AlguesResult"
            id={item.label}
            onChange={(e) => handleChange(e.target.value, item.value)}
            value={localStorage.getItem("paille")}
            style={{ backgroundColor: "#B0C220" }}
            disabled readonly
          /> */}
        </>
      );
      break;
    case "FoinResult":
      return (
        <>
          <table id="FirstResults">
            <tr
              style={{
                backgroundColor: "#B0C220",
                color: "white",
                borderRadius: 20,
              }}
            >
              <td>Paille d'avoine</td>
              <td>{localStorage.getItem("foin")}</td>
            </tr>
          </table>
          {/* <Form.Control
            type="FoinResult"
            id={item.label}
            onChange={(e) => handleChange(e.target.value, item.value)}
            value={localStorage.getItem("paille")}
            style={{ backgroundColor: "#B0C220" }}
            disabled readonly
          /> */}
        </>
      );
      break;
    case "TotalOne":
      return (
        <>
          <div id="ResultsDownTotal">
            <h3>Total</h3>
          </div>
          <table id="TableTotalResults">
            <tr
              style={{
                backgroundColor: "#B0C220",
                color: "white",
                borderRadius: 20,
              }}
            >
              <td style={{ padding: 20 }}>
                {Number(localStorage.getItem("paille")) +
                  Number(localStorage.getItem("algues")) +
                  Number(localStorage.getItem("foin"))}{" "}
                Nm3 Biogaz/an
              </td>
            </tr>
          </table>
          {/* <Form.Control
            type="TotalOne"
            id={item.label}
            onChange={(e) => handleChange(e.target.value, item.value)}
            value={localStorage.getItem("paille")}
            style={{ backgroundColor: "#B0C220" }}
            disabled readonly
          /> */}
        </>
      );
      break;
    case "TotalTwo":
      return (
        <>
          <table id="TableTotalResults">
            <tr
              style={{
                backgroundColor: "#B0C220",
                color: "white",
                borderRadius: 20,
              }}
            >
              <td style={{ padding: 20 }}>
                {(Number(localStorage.getItem("paille")) +
                  Number(localStorage.getItem("algues")) +
                  Number(localStorage.getItem("foin"))) /
                  365}{" "}
                Nm3 Biogaz/j
              </td>
            </tr>
          </table>
          {/* <Form.Control
            type="TotalTwo"
            id={item.label}
            onChange={(e) => handleChange(e.target.value, item.value)}
            value={localStorage.getItem("paille")}
            style={{ backgroundColor: "#B0C220" }}
            disabled readonly
          /> */}
        </>
      );
      break;
    case "TotalThree":
      return (
        <>
          <table id="TableTotalResults">
            <tr
              style={{
                backgroundColor: "#B0C220",
                color: "white",
                borderRadius: 20,
              }}
            >
              <td style={{ padding: 20 }}>
                {(Number(localStorage.getItem("paille")) +
                  Number(localStorage.getItem("algues")) +
                  Number(localStorage.getItem("foin"))) /
                  365 /
                  24}{" "}
                Nm3 Biogaz/h
              </td>
            </tr>
          </table>
          {/* <Form.Control
            type="TotalTwo"
            id={item.label}
            onChange={(e) => handleChange(e.target.value, item.value)}
            value={localStorage.getItem("paille")}
            style={{ backgroundColor: "#B0C220" }}
            disabled readonly
          /> */}
        </>
      );
      break;
    case "temps":
      return (
        <>
          <Form.Label>{item.label}</Form.Label>

          <Form.Control
            type="number"
            id={item.id}
            onChange={(e) => handleTempsChange(e.target.value, item.value)}
            value={currentValue}
            placeholder="Heures/an"
            required
          />

          <div id="errorTextTemps">
            <Form.Text style={{ color: "red" }}>Champs obligatoire</Form.Text>
          </div>
        </>
      );
      break;
    case "regime":
      return (
        <>
          <Form.Label>{item.label}</Form.Label>

          <Form.Control
            type="number"
            id={item.id}
            onChange={(e) => handleRegimeChange(e.target.value, item.value)}
            value={currentValue}
            placeholder="°C"
            required
          />

          <div id="errorTextRegime">
            <Form.Text style={{ color: "red" }}>Champs obligatoire</Form.Text>
          </div>
        </>
      );
      break;
    case "autonomie":
      return (
        <>
          <Form.Label>{item.label}</Form.Label>

          <Form.Control
            type="number"
            id={item.id}
            onChange={(e) => handleAutonomieChange(e.target.value, item.value)}
            value={currentValue}
            placeholder="X"
            required
          />

          <div id="errorTextAutonomie">
            <Form.Text style={{ color: "red" }}>Champs obligatoire</Form.Text>
          </div>
        </>
      );
      break;
    case "name":
      return (
        <>
          {/* <Form.Label >{item.label}</Form.Label> */}
          <Form.Control
            style={{ margin: 20 }}
            type="name"
            id={item.id}
            onChange={(e) => handleNameChange(e.target.value, item.value)}
            value={currentValue}
            placeholder="Nom"
          />
          <div id="errorTextUserName">
            <Form.Text style={{ color: "red" }}>Champs obligatoire</Form.Text>
          </div>
        </>
      );
      break;
    case "email":
      return (
        <>
          {/* <Form.Label >{item.label}</Form.Label> */}
          <Form.Control
            style={{ margin: 20 }}
            type="email"
            id={item.id}
            onChange={(e) => handleEmailChange(e.target.value, item.value)}
            value={currentValue}
            placeholder="Email"
          />
          <div id="errorTextUserEmail">
            <Form.Text style={{ color: "red" }}>Champs obligatoire</Form.Text>
          </div>
        </>
      );
      break;
    case "password":
      return (
        <>
          {/* <Form.Label htmlFor="inputPassword5">{item.label}</Form.Label> */}
          <Form.Control
            style={{ margin: 20 }}
            type="password"
            id="password"
            aria-describedby="passwordHelpBlock"
            onChange={(e) => handlePasswordChange(e.target.value, item.value)}
            placeholder="Mot de passe"
          />
          <div id="errorTextUserPassword">
            <Form.Text style={{ color: "red" }}>Champs obligatoire</Form.Text>
          </div>
        </>
      );
      break;
    case "ConfirmPassword":
      return (
        <>
          {/* <Form.Label htmlFor="inputPassword5">{item.label}</Form.Label> */}
          <Form.Control
            style={{ margin: 20 }}
            type="password"
            id="ConfirmPassword"
            aria-describedby="passwordHelpBlock"
            onChange={(e) =>
              handleConfirmPasswordChange(e.target.value, item.value)
            }
            placeholder="Confirmer le mot de passe"
          />
          <div id="errorTextUserPasswordConfirm">
            <Form.Text style={{ color: "red" }}>Champs obligatoire</Form.Text>
          </div>
        </>
      );
      break;
    case "information":
      return <p>{item.label}</p>;
    case "select":
      return (
        <div className="mt-2">
          <Form.Select
            aria-label={item.label}
            onChange={(e) => onChange(e.target.value, item.value)}
          >
            <option>{item.label}</option>
            {item.options.map((opt, index) => {
              return <option value={opt}>{opt}</option>;
            })}
          </Form.Select>
        </div>
      );

      return <></>;
  }
};
